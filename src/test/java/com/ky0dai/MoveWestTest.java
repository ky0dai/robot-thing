package com.ky0dai;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MoveWestTest {
    private static final Random RANDOM = new Random();
    private static final int UPPER_BOUND = 100;
    private static DecrementDistance decrement = new DecrementDistance(UPPER_BOUND,
            new EnforceUpperBound(UPPER_BOUND));
    private static final MoveEastAndWest MOVE_WEST = new MoveEastAndWest(decrement);

    @Test
    @Parameters
    public void moves_back_by_one(int initialX, int expectedX) {
        Robot robot = new Robot(Bearing.N, initialX, 1);
        Robot newPosition = MOVE_WEST.apply(robot, 1);
        assertThat(newPosition.getX(), is(expectedX));
    }

    @Test
    @Parameters
    public void moves_back_by_expected_distance(int initialX, int distanceTravelled, int expectedX) {
        Robot robot = new Robot(Bearing.N, initialX, 1);
        Robot newPosition = MOVE_WEST.apply(robot, distanceTravelled);
        assertThat(newPosition.getX(), is(expectedX));
    }

    private Object[] parametersForMoves_back_by_one() {
        Object[] objects = IntStream.range(1, 100).mapToObj(this::mapExpectedDistanceByOne).toArray();
        return ArrayUtils.add(objects, new Object[]{0, 99});
    }

    private Object[] mapExpectedDistanceByOne(int initial) {
        return new Object[]{initial, (initial- 1)};
    }

    private Object[] parametersForMoves_back_by_expected_distance() {
        Object[] objects = Stream.generate(this::testTupleSupplier)
                .limit(200)
                .toArray();

        // "lazy" cases; make these pass first
        var basic = new Object[] {30, 25, 5};
        var basicNegative = new Object [] {30, 35, 95};
        var extendedRangeNegative = new Object [] {30, 135, 95};

        return ArrayUtils.addAll(objects, basic, basicNegative, extendedRangeNegative);
    }

    private Object[] testTupleSupplier() {
        int initialX = RANDOM.nextInt(UPPER_BOUND);
        int randomDistance = RANDOM.nextInt(500);
        int rawCoordinate = initialX - randomDistance;
        int normalizedCoordinate = coordinateFromSubtractedDistance(rawCoordinate);
        return new Object[] {initialX, randomDistance, normalizedCoordinate};
    }

    private int coordinateFromSubtractedDistance(int initialX) {
        return initialX < 0 ? normalizeNegativeCoordinate(initialX)
                : initialX;
    }

    private int normalizeNegativeCoordinate(int initialX) {
        return UPPER_BOUND - normalizeWithinRange(initialX * -1);
    }

    private int normalizeWithinRange(int number) {
        int hundredthPlace = number / UPPER_BOUND;
        return number - (hundredthPlace * UPPER_BOUND);
    }
}
