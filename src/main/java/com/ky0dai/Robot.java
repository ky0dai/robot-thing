package com.ky0dai;

class Robot {
    private final Bearing bearing;
    private final int x;
    private final int y;


    public Robot(Bearing bearing, int x, int y) {
        this.bearing = bearing;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Bearing getBearing() {
        return bearing;
    }
}
