package com.ky0dai;

import java.util.function.Function;

public class EnforceUpperBound implements Function<Integer, Integer> {
    private final int upperBound;

    public EnforceUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    @Override
    public Integer apply(Integer number) {
        return number % upperBound;
    }
}
