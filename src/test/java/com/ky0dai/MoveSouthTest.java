package com.ky0dai;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MoveSouthTest {
    private static final Random RANDOM = new Random();
    private static final int UPPER_BOUND = 100;
    private static DecrementDistance decrement = new DecrementDistance(UPPER_BOUND,
            new EnforceUpperBound(UPPER_BOUND));
    private static final MoveNorthAndSouth MOVE_SOUTH = new MoveNorthAndSouth(decrement);

    @Test
    @Parameters
    public void moves_down_by_one(int initialY, int expectedY) {
        Robot robot = new Robot(Bearing.N, 1, initialY);
        Robot newPosition = MOVE_SOUTH.apply(robot, 1);
        assertThat(newPosition.getY(), is(expectedY));
    }

    @Test
    @Parameters
    public void moves_down_by_expected_distance(int initialY, int distanceTravelled, int expectedY) {
        Robot robot = new Robot(Bearing.N, 1, initialY);
        Robot newPosition = MOVE_SOUTH.apply(robot, distanceTravelled);
        assertThat(newPosition.getY(), is(expectedY));
    }

    private Object[] parametersForMoves_down_by_one() {
        Object[] objects = IntStream.range(1, 100).mapToObj(this::mapExpectedDistanceByOne).toArray();
        return ArrayUtils.add(objects, new Object[]{0, 99});
    }

    private Object[] mapExpectedDistanceByOne(int initial) {
        return new Object[]{initial, (initial- 1)};
    }

    private Object[] parametersForMoves_down_by_expected_distance() {
        Object[] objects = Stream.generate(this::testTupleSupplier)
                .limit(200)
                .toArray();

        // "lazy" cases; make these pass first
        var basic = new Object[] {30, 25, 5};
        var basicNegative = new Object [] {30, 35, 95};
        var extendedRangeNegative = new Object [] {30, 135, 95};

        return ArrayUtils.addAll(objects, basic, basicNegative, extendedRangeNegative);
    }

    private Object[] testTupleSupplier() {
        int initialY = RANDOM.nextInt(UPPER_BOUND);
        int randomDistance = RANDOM.nextInt(500);
        int rawCoordinate = initialY - randomDistance;
        int normalizedCoordinate = coordinateFromSubtractedDistance(rawCoordinate);
        return new Object[] {initialY, randomDistance, normalizedCoordinate};
    }

    private int coordinateFromSubtractedDistance(int initialX) {
        return initialX < 0 ? normalizeNegativeCoordinate(initialX)
                : initialX;
    }

    private int normalizeNegativeCoordinate(int initialX) {
        return UPPER_BOUND - normalizeWithinRange(initialX * -1);
    }

    private int normalizeWithinRange(int number) {
        int hundredthPlace = number / UPPER_BOUND;
        return number - (hundredthPlace * UPPER_BOUND);
    }

}