package com.ky0dai;

import java.util.function.BiFunction;
import java.util.function.Function;

class IncrementDistance implements BiFunction<Integer, Integer, Integer> {
    private final Function<Integer, Integer> enforceUpperBound;

    IncrementDistance(Function<Integer, Integer> enforceUpperBound) {
        this.enforceUpperBound = enforceUpperBound;
    }

    @Override
    public Integer apply(Integer initial, Integer distance) {
        int newCoordinate = initial + distance;

        return enforceUpperBound.apply(newCoordinate);
    }
}
