 ## ROBOT THING - Parameterized Test PoC

####What is this?
This is an implementation of Thoughtworks' classic "Mars Rover" problem that accepts an "input tape" and uses it to drive a robot around a grid!

This is intended to demonstrate:
 * Immutable functional programming style for fast, readable domain modeling
 * Parameterized test frameworks to cover lots of cases without writing a lot of test code
 * Property-based testing; asserting tests on rules rather than canned inputs, which may be incomplete. Useful blog: https://increment.com/testing/in-praise-of-property-based-testing/
