package com.ky0dai;

import java.util.function.BiFunction;

public class MoveEastAndWest implements BiFunction<Robot, Integer, Robot> {
    private final BiFunction<Integer, Integer, Integer> distanceTransformer;

    public MoveEastAndWest(BiFunction<Integer, Integer, Integer> distanceTransformer) {
        this.distanceTransformer = distanceTransformer;
    }

    @Override
    public Robot apply(Robot robot, Integer distance) {
        return new Robot(robot.getBearing(),
                distanceTransformer.apply(robot.getX(), distance),
                robot.getY());
    }
}
