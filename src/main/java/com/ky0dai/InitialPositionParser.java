package com.ky0dai;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

public class InitialPositionParser {
    private static final String INITIAL_POSITION_REGEX = "(?<bearing>[N|E|S|W]{1})\\s(?<x>[0-9]+)\\s(?<y>[0-9]+)";
    private static final String VALIDATION_REGEX = String.format("(%s)+", INITIAL_POSITION_REGEX);
    private static final Pattern INITIAL_POSITION_PATTERN = Pattern.compile(INITIAL_POSITION_REGEX);
    private static final Pattern VALIDATION_PATTERN = Pattern.compile(VALIDATION_REGEX);

    private final int gridSize;

    public InitialPositionParser(int gridSize) {
        this.gridSize = gridSize;
    }

    Robot parseInstructions(String instructionTape) {
        validate(instructionTape);

        Matcher parseMatcher = INITIAL_POSITION_PATTERN.matcher(instructionTape);
        while(parseMatcher.find()) {
            Bearing bearing = Bearing.valueOf(parseMatcher.group("bearing"));
            int x = parseInt(parseMatcher.group("x"));
            int y = parseInt(parseMatcher.group("y"));
            validateBounds(x);
            validateBounds(y);

            return new Robot(bearing, x, y);
        }

        raiseInvalidInputError();
        return null;
    }

    private void validateBounds(int length) {
        if(length > (gridSize-1)) {
            raiseInvalidInputError();
        }
    }

    private void validate(CharSequence instructionTape) {
        Matcher matcher = VALIDATION_PATTERN.matcher(instructionTape);
        boolean matches = matcher.matches();
        if(!matcher.matches()) {
            raiseInvalidInputError();
        }
    }

    private void raiseInvalidInputError() {
        throw new IllegalArgumentException("Initial position must start with [N|E|S|W] and supply starting X & Y coordinates .");
    }
}
