package com.ky0dai;

import java.util.function.Function;

import static java.lang.Integer.parseInt;

class DistanceParser implements Function<String, Integer> {

    @Override
    public Integer apply(String times) {
        return times.isEmpty() ?
                1 : parseInt(times);
    }
}
