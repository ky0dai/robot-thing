package com.ky0dai;

import java.util.Map;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Function;

public class App
{
    private static final int GRID_SIZE = 100;

    public static void main(String[] args )
    {
        var initialPositionParser = new InitialPositionParser(GRID_SIZE);

        var numberTimesParser = new DistanceParser();
        var instructionMap = initializeMap(GRID_SIZE);
        var instructionExecutor = new InstructionExecutor(instructionMap, numberTimesParser);
        var instructionParser = new InstructionParser(instructionExecutor);

        System.out.print("Initial position: ");
        var scanner = new Scanner(System.in);
        String initialStateString = scanner.nextLine();
        var robot = initialPositionParser.parseInstructions(initialStateString);
        System.out.println();

        String instructionTape = scanner.nextLine();
        var robotAfter = instructionParser.parseInstructions(robot, instructionTape);

        var finalState = String.format("Final Position: %s %d %d",
                robotAfter.getBearing(),
                robotAfter.getX(),
                robotAfter.getY());

        System.out.println(finalState);

    }

    private static Map<String, BiFunction<Robot, Integer, Robot>> initializeMap(int gridSize) {
        int cardinalLength = Bearing.values().length;
        var enforceUpperBoundRotate = new EnforceUpperBound(cardinalLength);
        var incrementRotation = new IncrementDistance(enforceUpperBoundRotate);
        var decrementRotation = new DecrementDistance(cardinalLength,
                enforceUpperBoundRotate);

        var rotateRight = new RotateTransform(new RotateEnum(incrementRotation));
        var rotateLeft = new RotateTransform(new RotateEnum(decrementRotation));

        var enforceUpperBoundDistance = new EnforceUpperBound(gridSize);
        var incrementDistance = new IncrementDistance(enforceUpperBoundDistance);
        var decrementDistance = new DecrementDistance(gridSize,
                enforceUpperBoundDistance);
        var moveNorth = new MoveNorthAndSouth(incrementDistance);
        var moveSouth = new MoveNorthAndSouth(decrementDistance);
        var moveEast = new MoveEastAndWest(incrementDistance);
        var moveWest = new MoveEastAndWest(decrementDistance);

        var movementByBearing = Map.of(Bearing.N, moveNorth,
                Bearing.S, moveSouth,
                Bearing.E, moveEast,
                Bearing.W, moveWest);

        var robotMover = new RobotMover(movementByBearing);


        return Map.of("R", rotateRight,
                "L", rotateLeft,
                "M", robotMover);
    }
}
