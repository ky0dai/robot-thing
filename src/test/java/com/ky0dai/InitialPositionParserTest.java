package com.ky0dai;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class InitialPositionParserTest {
    private static final String VALID_INPUT_NAIVE= "N 12 34";
    private static final String STARTS_WITH_NUMBER = 4 + VALID_INPUT_NAIVE;
    private static final String CONTAINS_EXTRA_WHITESPACE_MIDDLE = "N  12 34";
    private static final String CONTAINS_EXTRA_WHITESPACE_MIDDLE2 = "N 12  34";
    private static final String CONTAINS_UNEXPECTED_CHARACTER =  "N *12 34";
    private static final String CONTAINS_NEGATIVE_INPUT_X = "N -12 34";
    private static final String CONTAINS_NEGATIVE_INPUT_Y = "N 12 -34";
    private static final String CONTAINS_DOUBLE_DIRECTIONS_NO_SPACE = "NN 12 34";
    private static final String CONTAINS_DOUBLE_DIRECTIONS_WITH_SPACE = "E N 12 34";
    private static final String VALID_INPUT_OOB_X = "N 120 34";
    private static final String VALID_INPUT_OOB_Y = "N 20 100";
    public static final int GRID_SIZE = 100;

    private final InitialPositionParser initialPositionParser = new InitialPositionParser(GRID_SIZE);

    @Test
    @Parameters
    public void processes_successful_input(String input, Robot expectedRobot) {
        Robot robot = initialPositionParser.parseInstructions(input);
        assertThat(robot.getBearing(), is(expectedRobot.getBearing()));
        assertThat(robot.getX(), is(expectedRobot.getX()));
        assertThat(robot.getY(), is(expectedRobot.getY()));
    }


    @Test(expected = IllegalArgumentException.class)
    @Parameters
    public void rejects_invalid_input(String instructions) {
        initialPositionParser.parseInstructions(instructions);
    }

    private Object[] parametersForRejects_invalid_input() {
        return new Object[] {
                STARTS_WITH_NUMBER,
                CONTAINS_EXTRA_WHITESPACE_MIDDLE,
                CONTAINS_EXTRA_WHITESPACE_MIDDLE2,
                CONTAINS_UNEXPECTED_CHARACTER,
                CONTAINS_NEGATIVE_INPUT_X,
                CONTAINS_NEGATIVE_INPUT_Y,
                CONTAINS_DOUBLE_DIRECTIONS_NO_SPACE,
                CONTAINS_DOUBLE_DIRECTIONS_WITH_SPACE,
                VALID_INPUT_OOB_X,
                VALID_INPUT_OOB_Y
        };
    }

    private Object[] parametersForProcesses_successful_input() {
        var naiveBot = new Robot(Bearing.N, 12, 34);
        var naiveInput = new Object[]{VALID_INPUT_NAIVE, naiveBot};

        List<Object[]> pairs = new ArrayList<>();
        for(Bearing bearing: Bearing.values()) {
            for (int x = 0; x < GRID_SIZE; x = x+4) {
                for(int y = 0; y < GRID_SIZE; y = y + 3) {
                    Robot robot = new Robot(bearing, x, y);
                    String instruction = bearing + " " + x + " " + y;
                    pairs.add(new Object[]{instruction, robot});
                }
            }
        }

        return pairs.toArray();
    }
}