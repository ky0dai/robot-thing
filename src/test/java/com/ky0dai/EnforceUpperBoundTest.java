package com.ky0dai;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class EnforceUpperBoundTest {
    private static final Random RANDOM = new Random();

    @Test
    @Parameters
    public void enforces_upper_bound(int upperBound, int inputNumber, int expectedNumber) {
        EnforceUpperBound bound = new EnforceUpperBound(upperBound);
        Integer apply = bound.apply(inputNumber);
        assertThat(apply, is(expectedNumber));
    }

    private Object[] parametersForEnforces_upper_bound() {
        return Stream.generate(this::testTupleSupplier)
                .limit(5)
                .toArray();
    }

    private Object[] testTupleSupplier() {
        int bound = RANDOM.nextInt(500);
        int randomMultiplier = RANDOM.nextInt(500);
        int expectedNumber = RANDOM.nextInt(bound);
        int randomTestNumber = (bound * randomMultiplier) + expectedNumber;
        return new Object[]{bound,randomTestNumber, expectedNumber};
    }
}
