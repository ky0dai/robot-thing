package com.ky0dai;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/*
Property-based test to ensure that movement of the robot east complies with the following rules:
    1. Movement to the east should cause the 'X' map coordinate to increase by the distance travelled.
    2. If the distance travelled exceeds the size of the X-axis of the grid,
        the 'X' coordinate should wrap back to origin.
 */
@RunWith(JUnitParamsRunner.class)
public class MoveEastTest {
    private static final Random RANDOM = new Random();
    private static final int UPPER_BOUND = 100;
    private static final IncrementDistance INCREMENT = new IncrementDistance(new EnforceUpperBound(UPPER_BOUND));
    private static final MoveEastAndWest MOVE_EAST = new MoveEastAndWest(INCREMENT);

    @Test
    @Parameters
    public void moves_forward_by_one(int initialX, int expectedX) {
        Robot robot = new Robot(Bearing.N, initialX, 1);
        Robot newPosition = MOVE_EAST.apply(robot, 1);
        assertThat(newPosition.getX(), is(expectedX));
    }

    @Test
    @Parameters
    public void moves_forward_by_expected_distance(int initialX, int distanceTravelled, int expectedX) {
        Robot robot = new Robot(Bearing.N, initialX, 1);
        Robot newPosition = MOVE_EAST.apply(robot, distanceTravelled);
        assertThat(newPosition.getX(), is(expectedX));
    }

    private Object[] parametersForMoves_forward_by_one() {
        return IntStream.range(0, 100).mapToObj(this::mapExpectedDistanceByOne).toArray();
    }

    private Object[] mapExpectedDistanceByOne(int initial) {
        return new Object[]{initial, normalizeWithinRange(initial+ 1)};
    }

    private int normalizeWithinRange(int number) {
        return number % UPPER_BOUND;
    }

    private Object[] parametersForMoves_forward_by_expected_distance() {
        return  Stream.generate(this::testTupleSupplier)
                .limit(200)
                .toArray();
    }

    private Object[] testTupleSupplier() {
        int initialX = RANDOM.nextInt(UPPER_BOUND);
        int randomDistance = RANDOM.nextInt(500);
        int expectedPosition = normalizeWithinRange(initialX + randomDistance);
        return new Object[]{initialX, randomDistance, expectedPosition};
    }
}
