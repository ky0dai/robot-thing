package com.ky0dai;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

class InstructionExecutor {
    private final Map<String, BiFunction<Robot, Integer, Robot>> instructionMap;
    private final Function<String, Integer> numberTimesParser;

    InstructionExecutor(Map<String, BiFunction<Robot, Integer, Robot>> instructionMap,
                        Function<String, Integer> numberTimesParser) {
        this.instructionMap = instructionMap;
        this.numberTimesParser = numberTimesParser;
    }

    Robot applyInstruction(Robot initialState, String instruction, String times) {
        Integer distance = numberTimesParser.apply(times);
        return instructionMap.get(instruction).apply(initialState, distance);
    }
}
