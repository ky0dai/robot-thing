package com.ky0dai;

import java.util.function.BiFunction;

public class RotateEnum implements BiFunction<Bearing, Integer, Bearing> {
    private final BiFunction<Integer, Integer, Integer> rotationTransform;

    public RotateEnum(BiFunction<Integer, Integer, Integer> rotationTransform) {
        this.rotationTransform = rotationTransform;
    }

    @Override
    public Bearing apply(Bearing bearing, Integer distance) {
        var ordinal = bearing.ordinal();
        Integer apply = rotationTransform.apply(ordinal, distance);
        return Bearing.values()[apply];
    }
}
