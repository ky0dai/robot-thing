package com.ky0dai;

import java.util.function.BiFunction;

public class MoveNorthAndSouth implements BiFunction<Robot, Integer, Robot> {
    private final BiFunction<Integer, Integer, Integer> distanceTransformer;

    public MoveNorthAndSouth(BiFunction<Integer, Integer, Integer> distanceTransformer) {
        this.distanceTransformer = distanceTransformer;
    }

    @Override
    public Robot apply(Robot robot, Integer distance) {
        return new Robot(robot.getBearing(),
                robot.getX(),
                distanceTransformer.apply(robot.getY(), distance));
    }
}
