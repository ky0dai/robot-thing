package com.ky0dai;

import java.util.function.BiFunction;

public class RotateTransform implements BiFunction<Robot, Integer, Robot> {
    private final RotateEnum rotateBearingRight;

    public RotateTransform(RotateEnum rotateBearingRight) {
        this.rotateBearingRight = rotateBearingRight;
    }

    @Override
    public Robot apply(Robot robot, Integer distance) {
        return new Robot(rotateBearingRight.apply(robot.getBearing(), distance),
                robot.getX(),
                robot.getY());
    }
}
