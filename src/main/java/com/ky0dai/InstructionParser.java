package com.ky0dai;

import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class InstructionParser {
    private static final String INSTRUCTION_REGEX = "(?<command>[M|L|R]){1}(?<times>[0-9]*)";
    private static final String VALIDATION_REGEX = String.format("(%s)+", INSTRUCTION_REGEX);
    private static final Pattern VALIDATE_PATTERN = Pattern.compile(VALIDATION_REGEX);
    private static final Pattern PARSE_PATTERN = Pattern.compile(INSTRUCTION_REGEX);

    private final InstructionExecutor instructionExecutor;

    InstructionParser(InstructionExecutor instructionExecutor) {
        this.instructionExecutor = instructionExecutor;
    }

    Robot parseInstructions(Robot initialRobotState, String instructionTape) {
        validate(instructionTape);

        Matcher parseMatcher = PARSE_PATTERN.matcher(instructionTape);
        while(parseMatcher.find()) {
            String command = parseMatcher.group("command");
            String times = parseMatcher.group("times");
            initialRobotState = instructionExecutor.applyInstruction(initialRobotState, command, times);
        }

        return initialRobotState;
    }

    private void validate(CharSequence instructionTape) {
        Matcher matcher = VALIDATE_PATTERN.matcher(instructionTape);
        boolean matches = matcher.matches();
        if(!matcher.matches()) {
            throw new IllegalArgumentException("Instructions can only contain the letters M, L, R and numbers; cannot begin with a number or contain whitespace.");
        }
    }
}
