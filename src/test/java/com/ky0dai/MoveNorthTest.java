package com.ky0dai;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MoveNorthTest {
    private static final Random RANDOM = new Random();
    public static final int UPPER_BOUND = 100;
    private static IncrementDistance incrementDistance = new IncrementDistance(new EnforceUpperBound(UPPER_BOUND));
    private static final MoveNorthAndSouth MOVE_NORTH = new MoveNorthAndSouth(incrementDistance);

    @Test
    @Parameters
    public void moves_up_by_one(int initialY, int expectedY) {
        Robot robot = new Robot(Bearing.N, 1, initialY);
        Robot newPosition = MOVE_NORTH.apply(robot, 1);
        assertThat(newPosition.getY(), is(expectedY));
    }

    @Test
    @Parameters
    public void moves_up_by_expected_distance(int initialY, int distanceTravelled, int expectedY) {
        Robot robot = new Robot(Bearing.N, 1, initialY);
        Robot newPosition = MOVE_NORTH.apply(robot, distanceTravelled);
        assertThat(newPosition.getY(), is(expectedY));
    }

    private Object[] parametersForMoves_up_by_one() {
        return IntStream.range(0, 100).mapToObj(this::mapExpectedDistanceByOne).toArray();
    }

    private Object[] mapExpectedDistanceByOne(int initial) {
        return new Object[]{initial, normalizeWithinRange(initial+ 1)};
    }

    private int normalizeWithinRange(int number) {
        return number % UPPER_BOUND;
    }

    private Object[] parametersForMoves_up_by_expected_distance() {
        return Stream.generate(this::testTupleSupplier)
                .limit(200)
                .toArray();
    }

    private Object[] testTupleSupplier() {
        int initialY = RANDOM.nextInt(UPPER_BOUND);
        int randomDistance = RANDOM.nextInt(500);
        int expectedPosition = normalizeWithinRange(initialY + randomDistance);
        return new Object[]{initialY, randomDistance, expectedPosition};
    }
}