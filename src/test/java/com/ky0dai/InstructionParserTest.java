package com.ky0dai;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(JUnitParamsRunner.class)
public class InstructionParserTest {
    private static final String NAIVE_INPUT = "M123RLM1";
    private static final String STARTS_WITH_NUMBER = 4 + "M123RLM1";
    private static final String CONTAINS_WHITESPACE = "M12 3RLM1";
    private static final String CONTAINS_UNEXPECTED_CHARACTER = "A" + "M123RLM1";
    private static final String CONTAINS_NEGATIVE_INPUT = "M-123RLM1";
    private final InstructionExecutor instructionExecutor = mock(InstructionExecutor.class);
    private final Robot initialRobotState = new Robot(Bearing.N, 1, 1);
    private final InstructionParser generator = new InstructionParser(instructionExecutor);

    @Test
    public void parses_instructions() {
        when(instructionExecutor.applyInstruction(eq(initialRobotState), anyString(), anyString()))
                .thenReturn(initialRobotState);
        generator.parseInstructions(initialRobotState, NAIVE_INPUT);

        verify(instructionExecutor).applyInstruction(initialRobotState, "M", "123");
        verify(instructionExecutor).applyInstruction(initialRobotState, "R", "");
        verify(instructionExecutor).applyInstruction(initialRobotState, "L", "");
        verify(instructionExecutor).applyInstruction(initialRobotState, "M", "1");
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters
    public void rejects_invalid_input(String instructions) {
        generator.parseInstructions(initialRobotState, instructions);
    }

    private Object[] parametersForRejects_invalid_input() {
        return new Object[] {
                CONTAINS_UNEXPECTED_CHARACTER,
                STARTS_WITH_NUMBER,
                CONTAINS_WHITESPACE,
                CONTAINS_NEGATIVE_INPUT
        };
    }
}
