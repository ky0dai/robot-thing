package com.ky0dai;

import java.util.function.BiFunction;
import java.util.function.Function;

public class DecrementDistance implements BiFunction<Integer, Integer, Integer> {
    private final int upperBound;
    private final Function<Integer, Integer> enforceUpperBound;

    public DecrementDistance(int upperBound, Function<Integer, Integer> enforceUpperBound) {
        this.upperBound = upperBound;
        this.enforceUpperBound = enforceUpperBound;
    }

    @Override
    public Integer apply(Integer initial, Integer distance) {
        int newCoordinate = initial - distance;

        return newCoordinate < 0 ? applyNormalizedRetreatDistance(newCoordinate)
                : newCoordinate;
    }


    private int applyNormalizedRetreatDistance(int newCoordinate) {
        Integer integer = normalizeRetreatDistance(newCoordinate);
        return upperBound - integer;
    }

    private Integer normalizeRetreatDistance(int newCoordinate) {
        return enforceUpperBound.apply(-1 * newCoordinate);
    }
}
