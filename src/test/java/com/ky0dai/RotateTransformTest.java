package com.ky0dai;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class RotateTransformTest {
    private static final int UPPER_BOUND = Bearing.values().length;
    public static final EnforceUpperBound ENFORCE_UPPER_BOUND = new EnforceUpperBound(UPPER_BOUND);
    private static final IncrementDistance INCREMENT =
            new IncrementDistance(ENFORCE_UPPER_BOUND);
    private static final DecrementDistance DECREMENT =
            new DecrementDistance(UPPER_BOUND, ENFORCE_UPPER_BOUND);

    private final RotateTransform rotateRight = new RotateTransform(new RotateEnum(INCREMENT));
    private final RotateTransform rotateLeft = new RotateTransform(new RotateEnum(DECREMENT));

    @Test
    @Parameters
    public void moves_right_by_one(Bearing initialBearing, Bearing expectedBearing) {
        Robot robot = new Robot(initialBearing, 1, 1);
        Robot newPosition = rotateRight.apply(robot, 1);
        assertThat(newPosition.getBearing(), is(expectedBearing));
    }

    @Test
    @Parameters
    public void moves_left_by_one(Bearing initialBearing, Bearing expectedBearing) {
        Robot robot = new Robot(initialBearing, 1, 1);
        Robot newPosition = rotateLeft.apply(robot, 1);
        assertThat(newPosition.getBearing(), is(expectedBearing));
    }

    private Object[] parametersForMoves_right_by_one() {
        return new Object[] {
                new Object[]{Bearing.N, Bearing.E},
                new Object[]{Bearing.E, Bearing.S},
                new Object[]{Bearing.S, Bearing.W},
                new Object[]{Bearing.W, Bearing.N}
        };
    }

    private Object[] parametersForMoves_left_by_one() {
        return new Object[] {
                new Object[]{Bearing.N, Bearing.W},
                new Object[]{Bearing.E, Bearing.N},
                new Object[]{Bearing.S, Bearing.E},
                new Object[]{Bearing.W, Bearing.S}
        };
    }

}