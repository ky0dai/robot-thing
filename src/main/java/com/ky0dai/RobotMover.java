package com.ky0dai;

import java.util.Map;
import java.util.function.BiFunction;

public class RobotMover implements BiFunction<Robot, Integer, Robot> {
    private final Map<Bearing, BiFunction<Robot, Integer, Robot>> movementForBearingMap;

    public RobotMover(Map<Bearing, BiFunction<Robot, Integer, Robot>> movementForBearingMap) {
        this.movementForBearingMap = movementForBearingMap;
    }

    @Override
    public Robot apply(Robot robot, Integer integer) {
        BiFunction<Robot, Integer, Robot> robotIntegerRobotBiFunction = movementForBearingMap.get(robot.getBearing());
        return robotIntegerRobotBiFunction.apply(robot, integer);
    }
}
